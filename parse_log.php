<?php

function parse_date($str) {
    $parts = explode(":", $str);

    return substr($parts[0], 1);
}

function parse_line($line) {
    // I don't use regexps because of slower execution
    $data = explode(" ", trim($line));
    if (count($data) >= 4) {
        return array(
            "addr" => $data[0],
            "date" => parse_date($data[3])
        );
    } else {
        return null;
    }
}


function parse_file($path) {
    $visitor_hits = array();
    $days_data = array();

    foreach (explode("\n", file_get_contents($path)) as $k => $line) {
        if ($line == "")
            continue;

        $log = parse_line($line);
        $date = $log["date"];
        $addr = $log["addr"];

        if (array_key_exists($addr, $visitor_hits) === false) {
            $visitor_hits[$addr] = 0;
        }

        if (array_key_exists($date, $days_data) === false) {
            $days_data[$date] = array(
                "hits" => 0,
                "unique_visitors" => array(),
            );
        }

        $days_data[$date]["hits"]++;
        $visitor_hits[$addr]++;

        if (in_array($addr, $days_data[$date]["unique_visitors"]) === false) {
            $days_data[$date]["unique_visitors"][] = $addr;
        }

    }

    $all_hits = 0;
    $all_visitors = 0;
    foreach ($visitor_hits as $addr => $hits) {
        $all_visitors++;
        $all_hits += $hits;
    }

    return array(
        "hits_per_visitor" => $all_hits / $all_visitors,
        "days" => $days_data,
    );
}
