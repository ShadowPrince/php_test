<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <?php if ($page == "upload") {?>
        <form enctype="multipart/form-data" action="" method="POST">
        access.log: <input name="logfile" type="file" /> <input type="submit" value="Send File" />
        </form>
    <?php } else if ($page == "stats") { ?>
        <p>Days statistics:</p>
        <ul>
        <?php foreach ($data["days"] as $day => $array) { ?>
            <li><?= $day . " - " . $array["hits"] . " hits, " . count($array["unique_visitors"]) . " uniq visitors" ; ?></li>
        <?php } ?>
        </ul>

        <p>Hits per visitor: <?= $data["hits_per_visitor"]; ?></p>
    <?php } ?>
</body>
</html>
